< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase) 

epicsEnvSet("DEVICE_IP",                    "bd-mch00.cslab.esss.lu.se")
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_NAME",                  "MCH-000")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "IPMI")
# ?? epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

#- Create a asyn driver
drvAsynIPPortConfigure("$(PORT)","$(DEVICE_IP):623 udp", 0, 0, 0)
mchInit("$(PORT)")

# XXX consider sourcing iocsh files instead of defining the configuration
#     for every crate separately
#- load records for 3U crate with one SIS8300KU + DWC RTM
dbLoadRecords("mtca3u-crate-ess.db", "dev=$(PREFIX), link=$(PORT), location=$(LOCATION)")
dbLoadRecords("sis8300ku_ess.db", "dev=$(PREFIX), link=$(PORT), unit=1_, fruid=9")
dbLoadRecords("sis8300dwcrtm_ess.db", "dev=$(PREFIX), link=$(PORT), unit=1_, fruid=94")
#- load soft IOC admin records
dbLoadRecords("iocAdminSoft.db", "IOC=$(PREFIX)")

# Q: can autosave be used for this IOC?
#- set_requestfile_path("$(DB_DIR)")
#- set_requestfile_path("./")
#- set_savefile_path("$(AUTOSAVE_DIR)")
#- set_pass0_restoreFile("auto_settings.sav")
#- set_pass1_restoreFile("auto_settings.sav")
#- save_restoreSet_status_prefix("$(PREFIX)")
#- dbLoadRecords("save_restoreStatus.db","P=$(PREFIX)")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

###############################################################################
iocInit
###############################################################################

# Q: can autosave be used for this IOC?
#- save things every thirty seconds
#- create_monitor_set("auto_settings.req",30,"P=$(PREFIX),R=")

date
###############################################################################
