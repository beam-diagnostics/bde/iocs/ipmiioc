# IPMI monitoring

## 3U crate with one SIS8300-KU

```
nat> idb_info
IDB: info help
  [0]: print help
  [1]: print found devices
  [2]: print detailed info
Enter info mode (RET=1/0x1): 
IDB: print found devices
----------------------------------------------
FRU   type  i2c state sensors name
----------------------------------------------
  0   MCH3   80   M4      2   NMCH-CM
  1 ShFRU1   f4   M0      0   <none>
  2 ShFRU1   f4   M0      0   <none>
  3   MCH1   10   M4     13   NAT-MCH-MCMC
  6   AMC2   74   M4     13   CCT AM 900/412
  9   AMC5   7a   M4     20   SIS8300KU AMC
 10   AMC6   7c   M4     14   mTCA-EVR-300 
 31 ShFRU1   a6   M0      0   <none>
 40    CU1   a8   M4     14   Schroff uTCA CU
 50    PM1   c2   M4     31   NAT-PM-AC600D
 61   MCH1   16   M4     12   MCH-PCIe
 94   RTM5   7a   M4      5   <none>
253 ShFRU1   f4   M0      0   <Carrier FRU>
254 ShFRU1   f4   M0      0   <Shelf FRU>
----------------------------------------------
  found 14 devices
```

```
hinkokocevar@TPhinxx ~ $ ipmitool -H bd-mch04.cslab.esss.lu.se -P "" fru
Get HPM.x Capabilities request failed, compcode = c9
FRU Device Description : Builtin FRU Device (ID 0)
 Board Mfg Date        : Thu Dec 21 13:25:00 2017
 Board Mfg             : Schroff GmbH
 Board Product         : Schroff MicroTCA Backplane horizontal
 Board Serial          : 1481702265ZA
 Board Part Number     : 23005507
 Product Manufacturer  : Schroff GmbH
 Product Name          : Schroff MicroTCA Shelf
 Product Part Number   : 11890-193
 Product Version       : 01
 Product Serial        : 0000000000000001
 Product Asset Tag     : 0000000000000001

FRU Device Description : SIS8300KU RTM (ID 1)
 Chassis Type          : Rack Mount Chassis
 Chassis Part Number   : 11890193
 Chassis Serial        : 1511700175AA
 Board Mfg Date        : Thu Dec 21 12:25:00 2017
 Board Mfg             : Schroff GmbH
 Board Product         : Schroff MicroTCA Backplane
 Board Serial          : 1481702265ZA
 Board Part Number     : 23005507
 Product Manufacturer  : Schroff GmbH
 Product Name          : Schroff MicroTCA Shelf
 Product Part Number   : 0000000000000001
 Product Version       : 01
 Product Serial        : 0000000000000001
 Product Asset Tag     : 0000000000000001

FRU Device Description : CCT AM 900/412 (ID 6)
 Unsupported device

FRU Device Description : SIS8300KU AMC (ID 9)
 Unsupported device

FRU Device Description : Schroff uTCA CU (ID 40)
 Unsupported device

FRU Device Description : mTCA-EVR-300  (ID 10)
 Unsupported device

FRU Device Description : NAT-MCH-MCMC (ID 3)
 Unsupported device

FRU Device Description : MCH-Clock (ID 60)
 Unsupported device

FRU Device Description : MCH-PCIe (ID 61)
 Unsupported device

FRU Device Description : NAT-PM-AC600D (ID 50)
 Unsupported device
```
